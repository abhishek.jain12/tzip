---
tzip: 0xx
title: FA2.1
status: Draft
author: Lucas Felli (@Lfelli), Charles Dehlinger (@Charles.d)
type: Financial Application (FA)
created: 2022-08-10
date: 2021-28-02
version: 0.7
---

## Summary

TZIP-0xx proposes FA2.1 as an extension of the FA2 standard introduced in [TZIP-012](https://tzip.tezosagora.org/proposal/tzip-12/). FA2.1 adds support for on-chain views, tickets, events, and a finite allowance mechanism similar to FA1.2.

## Abstract

The goal of the FA2.1 extension is to include new protocol features while remaining fully compatible with current applications that rely on the FA2 standard.

**Events** are a means to solve discoverability problems encountered by indexers, i.e. to know when a token is transferred especially outside the specified transfer entrypoint (mint, burn, etc…).

**On-chain views** are a means for smart contracts to share information easily. Callback views are kept for compatibility with already deployed contracts.

**Tickets** are a way to materialize tokens outside of smart contracts. They can be transferred without calling the creator contract and affecting its status. Tezos rollups rely on tickets for representing Layer 1 assets on Layer 2.

Additionally, **finite allowance**, enabling a user to grant permission to spend a fixed amount of tokens, is introduced (based on the FA1.2 scheme).

## General

The key words “MUST”, “MUST NOT”, “REQUIRED”, “SHALL”, “SHALL NOT”, “SHOULD”, “SHOULD NOT”, “RECOMMENDED”, “MAY”, and “OPTIONAL” in this document are to be interpreted as described in [RFC 2119](https://www.ietf.org/rfc/rfc2119.txt).

FA2.1 MUST comply with the same rules defined by FA2 standard:

- Token type is uniquely identified on the chain by a pair composed of the token contract address and token ID, a natural number (`nat`). If the underlying contract implementation supports only a single token type (e.g. ERC-20-like contract), the token ID MUST be `0n`. In cases where multiple token types are supported within the same FA2.1 token contract (e. g. ERC-1155-like contract), the contract is fully responsible for assigning and managing token IDs.

- The FA2.1 batch entrypoints accept a list (batch) of parameters describing a single operation or a query. The batch MUST NOT be reordered or deduplicated and MUST be processed in the same order it is received.

- An empty batch is a valid input and MUST be processed as a non-empty one. For example, an empty transfer batch will not affect token balances, but applicable transfer core behavior and permission policy MUST be applied.

- If the underlying contract implementation supports only a single token type, the batch may contain zero or multiple entries where token ID is a fixed `0n` value. Likewise, if multiple token types are supported, the batch may contain zero or more entries and there may be duplicate token IDs.

## Extension specification

Token contracts implementing the FA2.1 standard MUST follow this interface:

- entrypoints:
  - balance_of (for backwards compatibility with FA2)
  - update_operators (for backwards compatibility with FA2)
  - transfer
  - approve
  - export_ticket
  - import_ticket
- views:
  - get_balance
  - get_total_supply
  - is_operator
  - get_allowance
  - get_token_metadata
  - is_token
- events:
  - transfer_event
  - balance_update
  - total_supply_update
  - operator_update
  - allowance_update
  - token_metadata_update

The FA2 entrypoints `balance_of` and `update_operators` should be deprecated at some point, but many applications rely on them so they MUST be implemented for backwards compatibility.

In some specific use cases, `export_ticket` and `import_ticket` may cause conflicts with the transfer policy. For this reason, one can simply throw an "FA2.1_TICKETS_UNSUPPORTED" error without implementing the logic of both entrypoints.

This standard relies on a specific type of ticket: `(pair nat (option bytes))` to represent assets. Be cautious to not reuse this type if you implement another custom ticket functionnality.

### Entrypoint semantics

#### `transfer`

The core behavior is the same as for FA2 with additional rules regarding allowances:

- To avoid conflicts between `approve` and `update_operators`, the operator's rights take precedence over those of the spender.
- If the allowance of the sender falls under zero, the transfer MUST fail  with the `"FA2.1_INSUFFICIENT_ALLOWANCE"` error message.
- By default, transfer of zero amount is allowed but beware of the behavior regarding allowance equals to `0` (or not set at all).

In the case of a self transfer (`%from_ == %to_`), make sure to not allow one to transfer more than they own, i.e. do not update `%to_`'s balance before `%from_`'s balance or check `%from_`'s balance is enough to transfer `%amount`.

#### `approve`

```Michelson
(list %approve
    (pair
        (address %owner)
        (address %spender)
        (nat %token_id)
        (or (nat %increase)
            (nat %decrease)
        )
    )
)
```

This entrypoint allows to increase or decrease the allowance set to `%spender` regarding a token `%owner` and a `%token_id`. The `%spender` can withdraw multiple times from the `%owner`'s balance up to the set value.

In case of a decrease below `0`, no error is thrown, the allowance MUST be removed or set to `0`. Beware of the case where anyone can transfer a `0` amount of tokens from anyone else because the default allowance is `0`.

Each call of `transfer` and `export_ticket` entrypoints decreases the allowance amount by the transferred amount of tokens, unless the transfer is called with `%from_` being equal to sender or with `%from_` being an operator on behalf of the sender.

The standard does not specify who is permitted to approve on behalf of the token owner. Depending on the business use case, the particular implementation of the FA2.1 contract MAY limit allowance updates to a token owner (`owner == SENDER`) or it can be limited to an administrator.

#### `export_ticket`

```Michelson
(list %export_ticket
    (pair
        (or %destination 
            key_hash 
            (contract 
                (list 
                    (pair
                        (option address)
                        (ticket (pair nat (option bytes)))
                    )
                )
            )
        )
        (list
            (pair
                (option %to_ address)
                (list %tickets_to_export
                    (pair
                       (address %from_)
                       (nat %token_id)
                       (nat %amount)
                    )
                )
            )
        )
    )
)
```

The ticket id MUST be a pair containing the `%token_id` and any additional information (a timestamp for example) as an `option bytes`. The ticket value MUST be the `%amount`.

In the case of the `destination` being a:

- `key_hash`, an operation is emitted for each exported ticket and the `key_hash` MUST be typed as a `(contract (ticket (pair nat (option bytes)))`. This allow tz addresses to receive tickets.
- `contract`, only one operation is necessary to transfer the tickets batched in a list.

The contract MUST fail with the error mnemonic `"FA2.1_INVALID_DESTINATION"` if the destination can't be correctly typed.

For contract receveiving the exported tickets, if needed the parameter `(option %to_ address)` SHOULD be used to associate the ticket to an address.

The entrypoint MUST comply with the same policy as the `transfer` entrypoint. The balance of `%destination` and `%to_` and the total supply of MUST remain unchanged.

#### `import_ticket`

```Michelson
(list %import_ticket
    (pair
        (option %to_ address)
        (list %tickets
            (ticket (pair nat (option bytes)))
        )
    )
)
```

The nat value of the ticket id of type `(pair nat (option bytes))` represents the `token_id` and the ticket value represents the amount to be credited to the `to_` address for the corresponding `token_id`. The ticket MUST be destroyed upon import.

If the ticket has not been created by the contract in which it is imported (`ticketer != SELF_ADDRESS`), the entrypoint MUST fail with the error mnemonic `"FA2.1_INVALID_TICKET"`.

If any of the `(option %to_ address)` is `none`, the entrypoint MUST fail with the error mnemonic `"FA2.1_INVALID_RECIPIENT"`.

The total supply MUST remain unchanged.

### Views

To let the view caller implements its own error handling logic, it's recommended to not fail inside a view but rather responding with a meaningful default value to let the callers choose their actions. For example, returning 0 when trying to get the balance of a non-existing wallet or a non-existing token id.

#### `get_balance`

Returns the number of tokens `%token_id` held for each `%owner` in the `%requests` list.

If `%owner` isn't known by the contract or `%token_id` doesn't exixt, the default returned value SHOULD be `0`.

```Michelson
view "get_balance" 
    (pair
      (address %owner)
      (nat %token_id)
    )
    (nat %balance)
```

#### `get_total_supply`

Returns the total supply of `token_id`. The total supply accounts for the number of tokens stored by the contract and the ones exported in tickets. i.e., exporting or importing tickets MUST not change the total supply.

If `%token_id` doesn't exixt, the default returned value SHOULD be `0`.

```Michelson
view "get_total_supply" 
    (nat %token_id)
    (nat %total_supply)
```

#### `is_operator`

Returns true if `%operator` is an operator of `%owner` for the token `%token_id`.

If `%owner` or `%operator` isn't known by the contract or `%token_id` doesn't exixt, the default returned value SHOULD be `False`.

```Michelson
view "is_operator" 
    (pair
      (address %owner)
      (address %operator)
      (nat %token_id)
    )
    (bool %is_operator)
```

#### `get_allowance`

Returns the number of tokens `%token_id` allowed by `%owner` for `%spender`.

If `%owner` or `%spender` isn't known by the contract or `%token_id` doesn't exixt, the default returned value SHOULD be `0`.

```Michelson
view "get_allowance" 
    (pair
      (address %owner)
      (address %spender)
      (nat %token_id)
    )
    (nat %allowance)
```

#### `get_token_metadata`

Returns the metadata associated to `%token_id`.

If `%token_id` doesn't exixt, the default returned value SHOULD be an empty `map %token_info`.

```Michelson
view "get_token_metadata" 
    (nat %token_id)
    (map %token_info string bytes)
```

#### `is_token`

Returns `True` if `%token_id` exists and `False` otherwise.

```Michelson
view "is_token" 
    (nat %token_id)
    (bool %token_exist)
```

### Events

To avoid confusion about the order of events, they MUST be emitted before further internal transactions if any.

#### `transfer_event`

MUST be triggered when a transfer occurs by any mechanism (a call to `transfer` but also custom entrypoints implementing a concept of transfer). The entrypoint `export_ticket` and `import_ticket` SHOULD NOT trigger this event.

```Michelson
(pair %transfer_event
    (address %from_)
    (address %to_)
    (nat %token_id)
    (nat %amount)
)
```

#### `balance_update`

MUST be triggered when the balance of a token owner is modified by any mechanism.

Only one `balance_update` event is allowed per pair (`%owner`, `%token_id`) per transaction.

`int %diff` represents the difference between `nat %new_balance` and the previous value (`new_balance = old_balance + diff`).

```Michelson
(pair %balance_update
    (address %owner)
    (nat %token_id)
    (nat %new_balance)
    (int %diff)
)
```

#### `total_supply_update`

MUST be triggered when the total supply of a `%token_id` is modified by any mechanism.

`int %diff` represents the difference between `nat %new_total_supply` and the previous value (`new_total_supply = old_total_supply + diff`). So a positive `%diff` is a mint and a negative `%diff` is a burn.

The list `(list (pair (%owner) (int %owner_diff)))` MUST contains every addresses affected by the total supply update where `%owner_diff` represents the effect on the `%owner`'s balance. The sum of all the `%owner_diff` may not be equal to `%diff` in the case of a creation or destruction of tickets `(ticket (pair nat bytes))`, in this case the list can even be empty.

Only one `total_supply_update` event is allowed per `%token_id` per transaction.

```Michelson
(pair %total_supply_update
    (list (pair (address %owner) (int %owner_diff)))
    (nat %token_id)
    (nat %new_total_supply)
    (int %diff)
)
```

#### `operator_update`

MUST be triggered when an operator is updated by any mechanism.

```Michelson
(pair %operator_update
    (address %owner)
    (address %operator)
    (nat %token_id)
    (bool %is_operator)
)
```

`%is_operator` is true if `%operator` has been set as an operator of `owner` and false otherwise.

#### `allowance_update`

MUST be triggered when a spender's allowance is updated by any mechanism (including when the allowance is consummed by a call to `transfer`).

`int %diff` represents the difference between `nat %new_allowance` and the previous value (`new_allowance = old_allowance + diff`).

```Michelson
(pair %allowance_update
    (address %owner)
    (address %spender)
    (nat %token_id)
    (nat %new_allowance)
    (int %diff)
)
```

#### `token_metadata_update`

MUST be triggered when a token metadata is updated by any mechanism, for example mint, burn, reveal, etc.

In case of the token is burned the `option %new_metadata (map string bytes)` MUST be `none`.

```Michelson
(pair %token_metadata_update
    (nat %token_id)
    (option %new_metadata (map string bytes))
)
```

### Error Handling

Following FA2 pattern:

| Error mnemonic                   | Description                                                                                           |
| :------------------------------- | :---------------------------------------------------------------------------------------------------- |
| `"FA2.1_INVALID_TICKET"`         | A ticket import failed because one of the ticket doesn't belong to the contract (`ticketer != SELF_ADDRESS`)  |
| `"FA2.1_INVALID_RECIPIENT"`      | A ticket import failed because one of the `option %to_ address` is `none` |
| `"FA2.1_INVALID_DESTINATION"`    | A destination contract can't be correctly typed to receive the exported tickets
| `"FA2.1_TICKETS_UNSUPPORTED"`    | Ticket export/import isn't supported                                                 |
| `"FA2.1_INSUFFICIENT_ALLOWANCE"` | A token spender does not have sufficient allowance to transfer tokens from owner's account           |
